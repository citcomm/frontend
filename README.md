# FrontendDev

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.7.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


# License

Dieses Projekt ist lizensiert unter der European Union Public Licence (EUPL) Version 1.2 oder neuer (ABl. EU L 128, 17. Mai 2017, S. 59; eupl.eu). Gemäß dieser Lizenz wird keine Gewährleistung gewährt und ein Haftungsausschluss (außer für Vorsatz und Personenschäden) vereinbart.

This project is licensed under the European Union Public Licence (EUPL) version 1.2 or later (OJEU, L 128, 19 May 2017, p. 59; eupl.eu). 
According to this license it comes without warranties and the Licensor will in no event be liable, except in the cases of wilful misconduct or damages directly caused to natural persons.

For the logos we used the font Latin Modern. This comes under the GUST Font License.