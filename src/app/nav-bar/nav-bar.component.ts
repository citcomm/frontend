import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  public InputLinkVisibility: boolean;
  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    console.log(this.route.toString());
    this.InputLinkVisibility = (this.route.toString() === 'Route(url:\'MainPage\', path:\'MainPage\')');
  }

}
