import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainpageListComponent } from './mainpage-list.component';

describe('MainpageListComponent', () => {
  let component: MainpageListComponent;
  let fixture: ComponentFixture<MainpageListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainpageListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainpageListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
