import {Component, Input, OnInit} from '@angular/core';
import {Measure} from '../models/measure';

@Component({
  selector: 'app-mainpage-list',
  templateUrl: './mainpage-list.component.html',
  styleUrls: ['./mainpage-list.component.css']
})
export class MainpageListComponent implements OnInit {

  @Input() measures: Measure[] = [];

  constructor() { }

  ngOnInit(): void {
  }
}
