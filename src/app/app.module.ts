import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MainpageComponent } from './mainpage/mainpage.component';
import { InputPageComponent } from './input-page/input-page.component';
import { HttpClientModule} from '@angular/common/http';

import { GoogleMapsModule } from '@angular/google-maps';
import { MapComponent } from './map/map.component';

import { MainpageListComponent } from './mainpage-list/mainpage-list.component';
import { MainpageListItemComponent } from './mainpage-list-item/mainpage-list-item.component';

import { AppRoutingModule } from './app-routing.module';
import { RouterModule, Routes } from '@angular/router';

import { DetailpageComponent } from './detailpage/detailpage.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';


const appRoutes: Routes = [
  {
    path: 'MainPage',
    component: MainpageComponent,
    data: {title: 'Main Page'}
  },
  {
    path: 'DetailPage/:id',
    component: DetailpageComponent,
    data: {title: 'Detail Page'}
  },
  {
    path: 'InputPage',
    component: InputPageComponent,
    data: {title: 'Input Page'}
  },
  {
    path: '',
    redirectTo: '/MainPage',
    pathMatch: 'full'
  },
];

@NgModule({
  declarations: [
    AppComponent,
    MainpageComponent,
    InputPageComponent,
    MapComponent,
    MainpageListComponent,
    MainpageListItemComponent,
    DetailpageComponent,
    NavBarComponent
  ],
    imports: [
        BrowserModule,
        GoogleMapsModule,
        AppRoutingModule,
        HttpClientModule,
        ReactiveFormsModule,
        RouterModule.forRoot(appRoutes),
        FormsModule
    ],
  exports: [
    RouterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

