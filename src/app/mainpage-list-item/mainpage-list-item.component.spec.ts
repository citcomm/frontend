import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainpageListItemComponent } from './mainpage-list-item.component';

describe('MainpageListItemComponent', () => {
  let component: MainpageListItemComponent;
  let fixture: ComponentFixture<MainpageListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainpageListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainpageListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
