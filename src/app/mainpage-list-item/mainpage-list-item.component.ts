import {Component, Input, OnInit} from '@angular/core';
import {Measure} from '../models/measure';

@Component({
  selector: 'app-mainpage-list-item',
  templateUrl: './mainpage-list-item.component.html',
  styleUrls: ['./mainpage-list-item.component.css']
})
export class MainpageListItemComponent implements OnInit {

  @Input() measure: Measure;

  constructor() { }

  ngOnInit(): void {
    console.log(this.measure);
  }
}
