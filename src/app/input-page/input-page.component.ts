import {Component, OnInit} from '@angular/core';
import {FormBuilder} from '@angular/forms';

@Component({
  selector: 'app-input-page',
  templateUrl: './input-page.component.html',
  styleUrls: ['./input-page.component.css']
})
export class InputPageComponent implements OnInit {
  form;


  constructor(private formBuilder: FormBuilder) {
    this.form = this.formBuilder.group({
      name: '',
      decreeAuthority: '',
      decreeDate: '',
      fileReference: '',
      validityDate: '',
      endDate: '',
      scope: '',
      legalNature: '',
      documentSource: '',
      tags: '',
      reason: ''
    });
  }

  ngOnInit(): void {
  }


  onSubmit(values: any) { // without type info
    console.log(values);
  }
}
