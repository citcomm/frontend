export class Scope {
  // der Geltungsbereich ist der kleinste gesetzte Wert (nicht leerer string) aus gemeinde < verband < kreis < bezirk
  id: number;
  municipality: string; // gemeinde
  federation: string;  // verband
  district: string;    // kreis
  county: string;   // bezirk
  state: string;     // Bundesland
  country: string;  // Land
}
