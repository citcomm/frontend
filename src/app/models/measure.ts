import {Scope} from './scope';

enum effect {
  suspends, extends, implements, changes, overlays, other
}

export class Measure {
  id: number; // intern
  name: string;
  decreeAuthority: string; // Erlassbehoerde
  fileReference: string;  // Aktenzeichen des Erlasses
  documentSource: string[]; // Link zum Quellen PdF
  scopes: Scope[];  // Geltungsbereiche der Measure
  decreeDate: string; // Datum des Erlasses
  validityDate: string; // Beginn der Measure
  endDate: string;  // Ende der Measure
  legalNature: string;  // Rechtsnatur der Measure
  tags: string[]; // Schlagwörter
  references: [Measure, effect][]; // referenziert diese Measures (von)
  referencedBy: [Measure, effect][]; // wird von diesen Measures referenziert (auf)
  reason: string; // Begründung der Maßnahme, entweder Text oder ein Link
}
