import {Component, Input, OnInit} from '@angular/core';
import {MeasureService} from '../services/measure.service';
import {Measure} from '../models/measure';
import {ScopeService} from '../services/scope.service';
import {Scope} from '../models/scope';

@Component({
  selector: 'app-mainpage',
  templateUrl: './mainpage.component.html',
  styleUrls: ['./mainpage.component.css']
})

export class MainpageComponent implements OnInit {

  @Input() public federalMeasures: Measure[] = [];
  @Input() public scopes: Scope[] = [];
  public filteredMeasures: Measure[] = [];
  public displayOldMeasures: boolean;

  constructor(private measureService: MeasureService, private scopeService: ScopeService) {
    this.displayOldMeasures = false;
  }

  ngOnInit(): void {
    this.measureService.getMeasuresFederal(this.displayOldMeasures)
      .subscribe((measures) => {
        this.federalMeasures = measures;
        this.filteredMeasures = this.federalMeasures;
      });
  }

  getScopesByName(term: string): void {
    const filter: any = {};
    filter.searchterm = term;
    if (term.length > 2) {
      this.scopeService.getScopes(filter)
        .subscribe((scopes) => {
          this.scopes = scopes;
        });
    } else if (term.length < 1) {
      this.filteredMeasures = this.federalMeasures;
    }
  }

// TODO: click on measure-element triggers this func
  getMeasuresByScope(id: number, oldMeasures: boolean = false): void {
    console.log('Filter by measure.id: ' + id);
    this.measureService.getMeasuresByScope(id)
      .subscribe((measures) => {
        this.filteredMeasures = measures;
      });
  }

  // get name of lowest layer in scope element, see Scope documentaion for more info
  scopeGetActualName(scope: Scope): string {
    if (scope.municipality !== undefined) {
      return scope.municipality;
    } else if (scope.federation !== undefined) {
      return scope.federation;
 } else if (scope.district !== undefined) {
      return scope.district;
 } else if (scope.county !== undefined) {
      return scope.county;
 } else if (scope.state !== undefined) {
      return scope.state;
 } else if (scope.country !== undefined) {
      return scope.country;
 }
  }

  test(value: string): void {
    console.log('test');
    console.log(value);
  }
}
