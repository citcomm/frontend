import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {MeasureService} from '../services/measure.service';
import {Measure} from '../models/measure';

@Component({
  selector: 'app-detailpage',
  templateUrl: './detailpage.component.html',
  styleUrls: ['./detailpage.component.css']
})
export class DetailpageComponent implements OnInit {
  public measure: Measure;
  @Input() id;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private measureService: MeasureService) {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.measureService.getMeasure(this.id).subscribe(measure => this.measure = measure);
  }

  ngOnInit(): void {
  }
}
