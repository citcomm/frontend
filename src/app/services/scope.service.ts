import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Scope} from '../models/scope';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ScopeService {

  protected url = 'https://citcomm.de/backend';


  constructor(private http: HttpClient) {
  }


  public getScopes(filter: any = {}): Observable<Scope[]> {
    console.log(this.url + '/scopes');
    return this.http.get<Scope[]>(this.url + '/scopes', {params: filter});
  }
}

