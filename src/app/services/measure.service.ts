import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Measure} from '../models/measure';

@Injectable({
  providedIn: 'root'
})
export class MeasureService {

  protected url = 'https://citcomm.de/backend';

  constructor(private http: HttpClient) {
  }

  public getMeasuresByScope(scopeId: any, oldMeasures: boolean = false): Observable<Measure[]> {
    console.log(this.url + '/measures/' + scopeId);
    return this.http.get<Measure[]>(this.url + `/measures/${scopeId}`);
  }

  public getMeasuresFederal(oldMeasures: boolean = false): Observable<Measure[]> {
    console.log(this.url + '/measures');
    return this.http.get<Measure[]>(this.url + `/measures`);
  }

  public getMeasure(measureId: any): Observable<Measure> {
    console.log(this.url + '/measure/' + measureId);
    return this.http.get<Measure>(this.url + `/measure/${measureId}`);
  }
}
